// swift-tools-version:5.3
import PackageDescription

let package = Package(
    name: "WebRTC",
    platforms: [.iOS(.v11)],
    products: [
        .library(
            name: "WebRTC",
            targets: ["WebRTC"]),
    ],
    dependencies: [ ],
    targets: [
        .binaryTarget(
            name: "WebRTC",
            url: "https://gitlab.com/evemeta/zdk/webrtc-sdk/uploads/bb8561792219dc08c2b6a29c1d407b6a/WebRTC.xcframework.zip",
            checksum: "0e69fba2bb315091264463f94044e94f0be352c82f6e5f72ec90f506a82697ac"
        ),
    ]
)